package it.auties.http.client;


import it.auties.http.model.HttpRequest;
import it.auties.http.model.HttpResponse;
import it.auties.http.model.HttpResponseStatus;
import it.auties.http.model.JsonBody;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.Validate;

import java.io.*;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

@Accessors
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EasyHttpClient {
  private final String userAgent;
  private final int maxDelay;

  public static EasyHttpClient newHttpClient(){
    return new EasyHttpClientBuilder()
      .maxDelay(Duration.ofSeconds(60))
      .userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.58")
      .build();
  }

  public static EasyHttpClientBuilder newBuilder(){
    return new EasyHttpClientBuilder();
  }

  public CompletableFuture<HttpResponse> sendAsync(HttpRequest request){
      return CompletableFuture.supplyAsync(() -> send(request));
  }

  public void send(HttpRequest request, Consumer<HttpResponse> callback){
    callback.accept(send(request));
  }

  public HttpResponse send(HttpRequest request){
    try {
      final HttpURLConnection connection = (HttpURLConnection) request.uri().toURL().openConnection();
      connection.setRequestMethod(request.method().name());
      connection.addRequestProperty("User-Agent", userAgent);
      connection.setReadTimeout(maxDelay);
      connection.setRequestProperty("Content-Length", Integer.toString(request.body().json().length()));
      connection.getOutputStream().write(request.body().json().getBytes(StandardCharsets.UTF_8));

      final var stream = new BufferedReader(new InputStreamReader(connection.getInputStream()));
      final var builder = new StringBuilder();
      var line = "";
      while ((line = stream.readLine()) != null) builder.append(line);
      stream.close();
      return new HttpResponse(new JsonBody(builder.toString()), HttpResponseStatus.forStatusCode(connection.getResponseCode()));
    }catch (IOException e){
      final var writer = new StringWriter();
      e.printStackTrace(new PrintWriter(writer));
      return new HttpResponse(new JsonBody(writer.toString()), HttpResponseStatus.FAILURE);
    }
  }

  public static class EasyHttpClientBuilder {
    private String userAgent;
    private Duration maxDelay;

    public EasyHttpClientBuilder userAgent(String userAgent){
      this.userAgent = userAgent;
      return this;
    }

    public EasyHttpClientBuilder maxDelay(Duration maxDelay){
      this.maxDelay = maxDelay;
      return this;
    }

    public EasyHttpClient build(){
      Validate.isTrue(userAgent != null, "Error while building EasyHttpClient instance: user agent cannot be null!");
      Validate.isTrue(!userAgent.isBlank(), "Error while building EasyHttpClient instance: user agent cannot be blank!");
      Validate.isTrue(maxDelay != null, "Error while building EasyHttpClient instance: max delay cannot be null!");
      Validate.isTrue(maxDelay.toMillis() > 0, "Error while building EasyHttpClient instance: max delay cannot be less than 0!");
      return new EasyHttpClient(userAgent, (int) maxDelay.toMillis());
    }
  }
}
