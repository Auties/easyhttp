package it.auties.http.model;

import java.util.Arrays;
import java.util.Optional;

public enum HttpMethod {
  POST,
  GET,
  PUT,
  PATCH,
  DELETE;

  public static Optional<HttpMethod> forName(String name){
    return Arrays.stream(values()).filter(entry -> entry.name().equals(name)).findAny();
  }
}
