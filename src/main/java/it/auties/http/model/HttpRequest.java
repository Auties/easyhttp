package it.auties.http.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.net.URI;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@Accessors(fluent = true)
public class HttpRequest {
  @Getter
  private final URI uri;
  @Getter
  private final HttpMethod method;
  @Getter
  private final JsonBody body;
}
