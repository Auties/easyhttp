package it.auties.http.model;


public record HttpResponse(JsonBody body, HttpResponseStatus status) {

}
