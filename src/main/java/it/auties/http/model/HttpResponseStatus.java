package it.auties.http.model;

public enum HttpResponseStatus {
  SUCCES,
  FAILURE;

  public static HttpResponseStatus forStatusCode(int code){
    return String.valueOf(code).startsWith("20") ? SUCCES : FAILURE;
  }
}
