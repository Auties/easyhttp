package it.auties.http.model;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.auties.http.utils.JsonUtils;
import lombok.SneakyThrows;

public record JsonBody(String json) {
  public static <T> JsonBody parse(T parsable){
    if(parsable instanceof JsonSerializableObject){
      return new JsonBody(((JsonSerializableObject) parsable).toJson());
    }

    return new JsonBody(JsonUtils.toJson(parsable));
  }

  public static JsonBody empty(){
    return new JsonBody("");
  }

  @SneakyThrows
  public <T> T as(){
    var writer = new ObjectMapper();
    return writer.readValue(json, new TypeReference<>() {});
  }

  @SneakyThrows
  public <T> T as(Class<T> clazz){
    var writer = new ObjectMapper();
    return writer.readValue(json, clazz);
  }
}
