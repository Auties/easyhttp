package it.auties.http.model;

import it.auties.http.utils.JsonUtils;
import lombok.SneakyThrows;

public interface JsonSerializableObject {
  @SneakyThrows
  default String toJson(){
    return JsonUtils.toJson(this);
  }
}
