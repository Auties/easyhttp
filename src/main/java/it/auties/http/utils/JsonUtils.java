package it.auties.http.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

@UtilityClass
public class JsonUtils {
  @SneakyThrows
  public String toJson(Object object){
    var writer = new ObjectMapper().writerWithDefaultPrettyPrinter();
    return writer.writeValueAsString(object);
  }
}
