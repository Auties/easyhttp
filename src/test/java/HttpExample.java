import it.auties.http.client.EasyHttpClient;
import it.auties.http.model.HttpMethod;
import it.auties.http.model.HttpRequest;
import it.auties.http.model.JsonBody;

import java.net.URI;

public class HttpExample {
  public static void main(String[] args) {
    final var body = new HttpTestBody("ale", "mario");
    final var request = HttpRequest
      .builder()
      .uri(URI.create("https://github.com/rzwitserloot/lombok/"))
      .method(HttpMethod.POST)
      .body(JsonBody.parse(body))
      .build();

    final var client = EasyHttpClient.newHttpClient();
    client
      .sendAsync(request)
      .thenApply((res) -> res.body().as(HttpTestResponse.class))
      .thenApply(HttpTestResponse::isFine)
      .thenAccept(System.out::println);
  }
}
