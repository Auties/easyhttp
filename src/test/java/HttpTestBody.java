import it.auties.http.model.JsonSerializableObject;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class HttpTestBody implements JsonSerializableObject {
  private String username;
  private String password;
}
